<?php
class Zend_Controller_Action_Helper_Haslo extends Zend_Controller_Action_Helper_Abstract{
	
	public function haslo($dlugosc_min, $dlugosc_max)
	{
  		// generowanie losowego słowa
  		$slowo = '';

		$slownik = APPLICATION_PATH . '/../library/resources/words';
  		$wp = @fopen($slownik, 'r');
  		if(!$wp)
    		return false; 
  		$wielkosc = filesize($slownik);

  		// przejœcie do losowej pozycji w słowniku
		srand ((double) microtime() * 1000000);
		$losowa_pozycja = rand(0, $wielkosc);
		fseek($wp, $losowa_pozycja);

		// pobranie ze słownika następnego pe³nego s³owa o w³aœciwej d³ugoœci
		while (strlen($slowo)< $dlugosc_min || strlen($slowo)>$dlugosc_max || strstr($slowo, "'"))
		{  
		   if (feof($wp))   
			  fseek($wp, 0);        // je¿eli koniec pliku, przeskocz na pocz¹tek
		   $slowo = fgets($wp, 80);  // przeskoczenie pierwszego s³owa bo mo¿e byæ niepe³ne
		   $slowo = fgets($wp, 80);  // potencjalne has³o
		};
		$slowo=trim($slowo); // obciêcie pocz¹tkowego \n z funkcji fgets
		srand ((double) microtime() * 1000000);
 		$losowa_liczba = rand(0, 999); 
 		$slowo .= $losowa_liczba;
		return $slowo;  
	  }
}