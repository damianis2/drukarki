<?php
require_once 'Zend/Controller/Action/Helper/Abstract.php';
class Zend_Controller_Action_Helper_ResizeImage extends Zend_Controller_Action_Helper_Abstract
{

	public function ResizeImage($imgName, $width, $height)
	{
		if(!($img = imagecreatefromjpeg($imgName))){
			echo("Nie mog� otworzy� pliku: $imgName");
			return false;
		}
		$sW = imagesx($img);
		$sH = imagesy($img);
		$tempImg = imagecreatetruecolor($width, $height);
		imagecopyresampled($tempImg, $img, 0, 0, 0, 0, $width, $height, $sW, $sH);
	
		imagejpeg($tempImg, 'resized'.$name);
	}
}
