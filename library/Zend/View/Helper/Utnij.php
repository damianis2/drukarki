<?php
class Zend_View_Helper_Utnij extends Zend_View_Helper_Action {
	public function utnij($tekst, $ile) {
		$licz = mb_strlen($tekst, 'utf-8');
		if ($licz>=$ile) {
			$tnij = mb_substr ( $tekst,0,$ile, 'UTF-8' );
		$uciete = $tnij."...";
		}
		else {
		$uciete = $tekst;
		}
		return $uciete;
	}
}