<?php
/* Helper wywołujący stopkę z automatyczną aktualizacją daty */
require_once 'Zend/Locale.php';

require_once 'Zend/View/Helper/Abstract.php';

class Zend_View_Helper_Stopka extends Zend_View_Helper_Abstract
{
 	public function stopka($rok, $opis)
	{
		$rok_zalozenia = (int)$rok; #ustalamy rok wyswietlania się witryny  
		$nazwa_strony = $opis; #ustalamy nazwe witryny 
		$rok_obecny = date("Y"); #z serwera zczytujemy obecny rok 
		if ($rok_obecny == $rok_zalozenia) { #porównujemy wartosci zmiennych 
			$t = "Copyright &copy; ". $rok_obecny .", ". $nazwa_strony .""; #komunikat jesli strona powstala obecnym roku 
		} else { 
			$t = "Copyright &copy; ". $rok_zalozenia ." - ". $rok_obecny .", ". $nazwa_strony ."";#komunikat jesli strona jest starsza 
		} 	
		return $t;
	}
}
