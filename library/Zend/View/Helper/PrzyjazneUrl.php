<?php
class Zend_View_Helper_PrzyjazneURL extends Zend_View_Helper_Action {
	public function przyjazneURL($spacje){
		$from=array(' ','ę','ó','ą','ś','ł','ż','ź','ć','ń','?','.',',',';',':'
		,'Ę','Ó','Ą','Ś','Ł','Ż','Ź','Ć','Ń');
		$to=array('-','e','o','a','s','l','z','z','c','n','-','-','-','-','-'
		,'E','O','A','S','L','Z','Z','C','N');
		$spacje = str_replace($from, $to, $spacje);
		return strtolower($spacje);
	}
}