<?php
class Zend_View_Helper_Netto extends Zend_View_Helper_Action {

	public function netto($cena)
	{
		
		#Wyciąg danych o podwyżce z bazy
		$ustawienia = new Application_Model_Ustawienia();
		$stan = $ustawienia->pokazUstawienia('16');
		
		$podatek = ($stan[wartosc] / 100) + 1;
		
		$cenaNetto = $cena/$podatek; 
		
		return number_format($cenaNetto,2,',',' ');
	}
}