<?php
class Zend_View_Helper_Pasek extends Zend_View_Helper_Action {

	public function Pasek($add = array(),$print = TRUE, $facebook = TRUE) {

		$html = '<div id="pasek" class="druk_false"><div style="font-size: 10px; color: #817a7a; width: 500px; float:left;" >
			<div class="home"></div>
			<div style="display:inline-block; position:relative; top: -3px;">
				<a href="http://www.goldrun.pl"><strong>www.goldrun.pl</strong></a>';
				
				if(count($add) != 0)
				foreach($add as $ad){
					$html .= ' &raquo; ';
					$html .= ' <a href="'.$ad[url].'">'.$ad[name].'</a> ';
				}
				
			$html .= '</div> 
		</div>';
		if($print == TRUE){ 
		$html .= '<a class="print" style="float:right;" onclick="windows:print();"></a>';
		}
		if($facebook == TRUE){
		$html .= '<a href="" class="facebook" style="float:right;"></a>';
		}
		$html .= '<a style="cursor:pointer; float:right; margin-top: 2px;" onclick="history.go(-1);">&laquo; Powrót | </a> 
		<div style="clear:both;"></div>
		<hr />
		</div>';
		return $html;
	}
}