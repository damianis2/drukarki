<?php
class Zend_View_Helper_TwardaSpacja extends Zend_View_Helper_Action {
	public function twardaSpacja($literowki){ 
	 $from=array(' d ', ' w ',' z ',' i ',' a ',' W ',' Z ',' I ',' A ');
	 $to=array(' d&nbsp;', ' w&nbsp;',' z&nbsp;',' i&nbsp;',' a&nbsp;',' W&nbsp;',' Z&nbsp;',' I&nbsp;',' A&nbsp;');
	 $literowki = str_replace($from, $to, $literowki); 
	 return $literowki;
	}
}	
	