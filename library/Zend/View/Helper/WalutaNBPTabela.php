<?php
class Zend_View_Helper_WalutaNBPTabela extends Zend_View_Helper_Action {
	public function walutaNBPTabela() {
		$xml = simplexml_load_file("http://www.nbp.pl/Kursy/xml/LastC.xml");

		$chld = $xml->children();
		$numer_tabeli = $chld[0];
		$data_notowania = $chld[1];
		$data_publikacji = $chld[2];
		
		foreach($xml->children() as $child)
		{
			if($child == 'numer_tabeli')
			{
				$numer_tabeli = $child;
			}
			if($child == 'data_notowania')
			{
				$data_notowania = $child;
			}
			if($child == 'data_publikacji')
			{
				$data_publikacji = $child;
			}
			if(is_array($child))
			{
				echo '0';
			} else {
				foreach($child as $key => $value)
			{
				if($key == 'nazwa_waluty')
				{
				$nazwa_waluty[count($nazwa_waluty)] = $value;
				}
				if($key == 'przelicznik')
				{
				$przelicznik[count($przelicznik)] = $value;
				}
				if($key == 'kod_waluty')
				{
				$kod_waluty[count($kod_waluty)] = $value;
				}
				if($key == 'kurs_kupna')
				{
				$kurs_kupna[count($kurs_kupna)] = $value;
				}
				if($key == 'kurs_sprzedazy')
				{
				$kurs_sprzedazy[count($kurs_sprzedazy)] = $value;
				}
			}
		}
	}
	?>
    <table border="1" class="zam">
    <tr><th>Waluta</th><th>Skrót</th><th>Kupno</th><th>Sprzedaż</th></tr>
    <?php
    for($i=0; $i<=count($nazwa_waluty)-1; $i++)
    {
    echo '<tr><td>'.$nazwa_waluty[$i].'</td><td>'.$kod_waluty[$i].'</td><td>'.$kurs_kupna[$i].' zł</td><td>'.$kurs_sprzedazy[$i].' zł</td></tr>
    ';
    }
    ?>
    </table>
    <?php
}
}