<?php
class Zend_View_Helper_Cena extends Zend_View_Helper_Action {

	public function cena($id,$dzial = 1)
	{
		
		#Wyciąg danych o podwyżce z bazy
		$produkty = new Application_Model_DbTable_Produkty();
		$produkt = $produkty->podwyzkaPokaz($id);
		
		#wyciąganie ceny danego produktu
		$cen = new Application_Model_DbTable_ProduktyCena();
		
		$p = Zend_Controller_Front::getInstance()->getRequest()->getParam('proba');
		if(empty($p)){
			$probaa = 2;
		}else{
			$probaa = Zend_Controller_Front::getInstance()->getRequest()->getParam('proba');
		}
		$c = $cen->pokaz($id,$probaa,0);
		

		
		$cena = (float)str_replace(',','.',$c[cena]);		
		
		#Parametry
		if($_POST[srodek] == 'polokragly')
		$cena = $cena * 1.20;
		elseif($_POST[srodek] == 'klasyczny')
		$cena;
		#end parametry
		
		# PODWYŻKA PRODUKTU
		switch($produkt[podwyzka]){
			case 0 : // Wyłączona
				$cena = $cena;
			break; 
			case 1 : // Podwyżka Ręcznie
				$cena = $cena+$produkt[podwyzka_recznie];
			break; 
			case 2 : // Podwyżka Procent
				$cena = $cena*(($produkt[podwyzka_procent]/100)+1);
			break; 
		}
		# KONIEC PODWYŻKA PRODUKTU
		
		# PODWYŻKA KOLEKCJA
		$kolekcje = new Application_Model_Kolekcje();
		$k = $kolekcje->pokaz2($produkt[id_kolekcja]);
		
		switch($k[podwyzka]){
			case 0 : // Wyłączona
				$cena = $cena;
			break; 
			case 1 : // Podwyżka Ręcznie
				$cena = $cena+$k[podwyzka_recznie];
			break; 
			case 2 : // Podwyżka Procent
				$cena = $cena*(($k[podwyzka_procent]/100)+1);
			break; 
		}
		# KONIEC PODWYŻKA KOLEKCJA
		
		$ustawienia = new Application_Model_Ustawienia();
		switch($produkt[id_dzial]){
			case 1 :
				$podwyzka_cen = $ustawienia->pokazUstawienia('17');
			break;
			case 2 :
				$podwyzka_cen = $ustawienia->pokazUstawienia('20');
			break;
			case 3 :
				$podwyzka_cen = $ustawienia->pokazUstawienia('23');
			break;
		}

		# PODWYŻKA Kolekcji	
		switch((int)$podwyzka_cen[wartosc]){
			case 0 : // Wyłączona
			break; 
			case 1 : // Podwyżka Ręcznie
				$podwyzka_cen_recznie = $ustawienia->pokazUstawienia('18');
				$cena = $cena+ $podwyzka_cen_recznie[wartosc];
			break; 
			case 2 : // Podwyżka Procent
				$podwyzka_cen_procent = $ustawienia->pokazUstawienia('19');
				$cena = $cena*(($podwyzka_cen_procent[wartosc]/100)+1);
			break; 
		}
		
		# KONIEC PODWYŻKA KOLEKCJI
	
		# PODWYŻKA CAŁKOWITA	
		$podwyzka = $ustawienia->pokazUstawienia('13');
		
		switch((int)$podwyzka[wartosc]){
			case 0 : // Wyłączona
			break; 
			case 1 : // Podwyżka Ręcznie
				$podwyzka_recznie = $ustawienia->pokazUstawienia('14');
				$cena = $cena+ $podwyzka_recznie[wartosc];
			break; 
			case 2 : // Podwyżka Procent
				$podwyzka_procent = $ustawienia->pokazUstawienia('15');
				$cena = $cena*(($podwyzka_procent[wartosc]/100)+1);
			break; 
		}
		# KONIEC PODWYŻKA CAŁKOWITA
		
		$stan = $ustawienia->pokazUstawienia('10');
    	$GBP_akt = (float)$stan[wartosc];
		
		$stan = $ustawienia->pokazUstawienia('11');
    	$USD_akt = (float)$stan[wartosc];
		
		$stan = $ustawienia->pokazUstawienia('12');
    	$EURO_akt = (float)$stan[wartosc];
		
		$waluta = $ustawienia->pokazUstawienia('6');
    	$waluta = (float)$waluta[wartosc];
		
		if((int)$waluta == 1){
			
			$gbp = $ustawienia->pokazUstawienia('7');
			$gbp = (float)$gbp[wartosc];
			
			$usd = $ustawienia->pokazUstawienia('8');
			$usd = (float)$usd[wartosc];
			
			$euro = $ustawienia->pokazUstawienia('9');
			$euro = (float)$euro[wartosc];
			
			$gbp = number_format(($cena/$gbp),0,',',' ');
			$usd = number_format(($cena/$usd),0,',',' ');
			$euro = number_format(($cena/$euro),0,',',' ');
		}elseif((int)$waluta == 0){
			$waluty = Zend_View_Helper_WalutaNBP::walutaNBP($cena);
			$gbp = number_format($waluty[gbp],0,',',' ');
			$usd = number_format($waluty[usd],0,',',' ');
			$euro = number_format($waluty[euro],0,',',' ');
		}		
		
		$pr[cena_zl] = $cena;
			
			if((int)$GBP_akt == 1){
				$pr[cena_gbp] = (float)$gbp;
			}
			
			if((int)$USD_akt == 1){
				$pr[cena_usd] = (float)$usd;
			}
			
			if((int)$EURO_akt == 1){
				$pr[cena_euro] = (float)$euro;
			}
		
		return $pr;
	}
}