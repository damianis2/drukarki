<?php
class Zend_View_Helper_WalutaNBP extends Zend_View_Helper_Action {
	public function walutaNBP($cena) {
		$xml = simplexml_load_file("http://www.nbp.pl/Kursy/xml/LastC.xml");

		$chld = $xml->children();
		$numer_tabeli = $chld[0];
		$data_notowania = $chld[1];
		$data_publikacji = $chld[2];
		
		foreach($xml->children() as $child)
		{
			if($child == 'numer_tabeli')
			{
				$numer_tabeli = $child;
			}
			if($child == 'data_notowania')
			{
				$data_notowania = $child;
			}
			if($child == 'data_publikacji')
			{
				$data_publikacji = $child;
			}
			if(is_array($child))
			{
				echo '0';
			} else {
				foreach($child as $key => $value)
			{
				if($key == 'nazwa_waluty')
				{
				$nazwa_waluty[count($nazwa_waluty)] = $value;
				}
				if($key == 'przelicznik')
				{
				$przelicznik[count($przelicznik)] = $value;
				}
				if($key == 'kod_waluty')
				{
				$kod_waluty[count($kod_waluty)] = $value;
				}
				if($key == 'kurs_kupna')
				{
				$kurs_kupna[count($kurs_kupna)] = $value;
				}
				if($key == 'kurs_sprzedazy')
				{
				$kurs_sprzedazy[count($kurs_sprzedazy)] = $value;
				}
			}
		}
	}
	$zloty = $cena;
	
	$usd = str_replace(',','.', $kurs_kupna[0]);
	$euro = str_replace(',','.', $kurs_kupna[3]);
	$gbp = str_replace(',','.', $kurs_kupna[6]);

		
		$d = ($usd);
		$e = ($euro);
		$g = ($gbp);
		
		$kwoty[gbp] = $g;
		$kwoty[usd] = $d;
		$kwoty[euro] = $e;
		
		return $kwoty;
	}
}